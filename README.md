# Kubernetes/Containerization Notes

## History of Containers

Containers have existed for a number of years.  Have had many forms.

Container definition: A collection of software process unified by one namespace, with access to an operting system kernel that it ahsres with other containers and little to no access between containers.

Docker modifies this by saying that a runtime instance of a docker image contains 3 things:

1. A docker image
2. An execution environment
3. A standard set of instructions

**Core Elments of a Docker Ecosystem**  

***Docker Engine***  
  * Comprised of the runtime and packaging tool
  * **Must** be installed on the hosts that run Docker

***Docker Store***
  * An online cloud service where users can store and share their Docker images
  * Also know as Docker Hub

Difference between Container and a VM:

_VMs Have_  
* One or many applications
* The necessary binaries and libraries
* The entire guest operating system to interact with the applications

_Containers Have_
* The application and all of its dependencies  
* Shares the kernel with other containers  
* Not tied to infrastructure - only needs Docker Engine install on the host  
* Run isolated process on the host OS  
* Provide benefits to Developers  
  * Applications are portable  
  * Applications are packaged ina  standard way  
* Deployment is  
  * Easy  
  * Repeatable  
* Testing automating and packaging become easier.  
* Deployments become much more reliable  
* Process becomes repeatable  
* No more differences between dev, testing, and deployment environments  
* Languages between members of devops team becomes common.  

Refer to the [Forrester White Paper](Containers_Real_Adoption_2017_Dell_EMC_Forrester_Paper.pdf) for container use-cases and projections of future uses

## Kubernetes

_Definition_: an open-source platform designed to automate the deploying, scaling, and operating application containers

_Goal_: foster an ecosystem of components and tools that relieve the burden of running applications in public and private clouds.

How do you manage multiple running containers on or across many hosts?

Features required to do this:
* Provision hosts
* Instantiate containers on a host
* Restart failing containers
* Expose container as services outside the cluster
* Scale the cluster up or down

### Kubernetes and Docker

Kubernetes is a container platform.  You can use Docker containers to develop and build applications, and then use Kubernetes to run these applications on your infrastructure.

* How do we build a development platform for UAGC using Kubernetes?

Four biggest players in Container Orchestration:  
1. Kubernetes
2. Rancher
3. Docker Swarm
4. Mesos

### Architecture of Kubernetes Node

#### Master Node

Responsible for overall management of K8s cluster.
Three components that handle comm, scheduling and controllers: API Server,  Scheduler, Controller Manager

![Graphic of Entire Kubernetes Architecture](kubernetes-architecture.png)  

[Logging Document to Review](https://theagileadmin.com/2010/08/20/logging-for-success/) 

